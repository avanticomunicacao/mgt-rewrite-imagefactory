var config = {
     paths: {
         "lazyload": 'Avanti_RewriteImageFactory/js/lazyload'
     },
     shim: {
         'lazyload': {
             'deps': ['jquery']
         },
     }
 };